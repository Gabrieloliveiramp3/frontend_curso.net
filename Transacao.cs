using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace MudBlazor
{
    public class Transacao
    {
        public int Id { get; set; }

        public int UsuarioId { get; set;}

        public string Descricao { get; set;}

        public double Valor { get; set;}

        public int CategoriaId { get; set;}

        public int Recorrencia { get; set;}

        public bool Tipo { get; set;}

        public DateTime? DataTransa { get; set;} 

        public Transacao(int id, int usuarioId, string descricao, double valor, int categoriaId, int recorrencia, bool tipo, DateTime datatransa){
            Id = id;
            UsuarioId = usuarioId;
            Descricao = descricao;
            Valor = valor;
            CategoriaId = categoriaId;
            Recorrencia = recorrencia;
            Tipo = tipo;
            DataTransa = datatransa;
        }

    }

    

}